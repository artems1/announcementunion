require "test_helper"

class StaticPagesControllerTest < ActionController::TestCase #< ActionDispatch::IntegrationTest
  test "should get home" do
    #get root_path
    get :home
    assert_response :success
    assert_select "title", "Ruby on Rails Tutorial Sample App"
  end

  test "should get help" do
    #get static_pages_help_url
    get :help
    assert_response :success
    assert_select "title", "Help | Ruby on Rails Tutorial Sample App"
  end
  
   test "should get about" do
    #get static_pages_about_url
    get :about
    assert_response :success
    assert_select "title", "About | Ruby on Rails Tutorial Sample App"
  end
  
    test "should get contact" do
    #get static_pages_contact_url
    get :contact
    assert_response :success
    assert_select "title", "Contact | Ruby on Rails Tutorial Sample App"
  end
  
end
