require 'test_helper'

class AnnouncementsControllerTest < ActionController::TestCase

  def setup
    @announcements = announcements(:orange)
  end

  test "should redirect create when not logged in" do
    assert_no_difference 'Announcement.count' do
      post :create, params: { announcements: { content: "Lorem ipsum" } }
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'Announcement.count' do
      delete :destroy, params: { id: @announcements }
    end
    assert_redirected_to login_url
  end
  
  test "should redirect destroy for wrong announcement" do
    log_in_as(users(:michael))
    announcement = announcements(:ants)
    assert_no_difference 'Announcement.count' do
      delete :destroy, params: {id: announcement}
    end
    assert_redirected_to root_url
  end

end