require 'test_helper'

class AnnouncementsInterfaceTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
  end

  test "announcement interface" do
    log_in_as(@user)
    get root_path
    assert_select 'div.pagination'
    # Невалидная отправка формы.
    assert_no_difference 'Announcement.count' do
      post announcements_path, params: {announcement: { content: "" }}
    end
    assert_select 'div#error_explanation'
    # Валидная отправка формы.
    content = "This announcement really ties the room together"
    assert_difference 'Announcement.count', 1 do
      post announcements_path, params: { announcement: { content: content }}
    end
    assert_redirected_to root_url
    follow_redirect!
    assert_match content, response.body
    # Удаление сообщения.
    assert_select 'a', text: 'delete'
    first_announcement = @user.announcements.paginate(page: 1).first
    assert_difference 'Announcement.count', -1 do
      delete announcement_path(first_announcement)
    end
    # Посещение профиля другого пользователя.
    get user_path(users(:archer))
    assert_select 'a', text: 'delete', count: 0
  end
end