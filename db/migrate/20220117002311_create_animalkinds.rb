class CreateAnimalkinds < ActiveRecord::Migration[6.1]
  def change
    create_table :animalkinds do |t|
      t.string :kind

      t.timestamps null: false
    end
  end
end
