class AddPictureToAnnouncements < ActiveRecord::Migration[6.1]
  def change
    add_column :announcements, :picture, :string
  end
end
