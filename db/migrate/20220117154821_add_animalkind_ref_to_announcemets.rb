class AddAnimalkindRefToAnnouncemets < ActiveRecord::Migration[6.1]
  change_table :announcements do |t|
      t.references :animalkind, index: true, foreign_key: true
  end
end
