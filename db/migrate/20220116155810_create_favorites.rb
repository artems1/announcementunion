class CreateFavorites < ActiveRecord::Migration[6.1]
  def change
    create_table :favorites do |t|
      t.integer :announcement_id
      t.references :user, foreign_key: true

      t.timestamps null: false
    end
        add_index :favorites, [:user_id, :announcement_id], unique: true
  end
end
