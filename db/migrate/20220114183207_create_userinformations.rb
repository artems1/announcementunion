class CreateUserinformations < ActiveRecord::Migration[6.1]
  def change
    create_table :userinformations do |t|
      t.string :firstname
      t.string :secondname
      t.string :phone
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
