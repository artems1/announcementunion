class AddCategoryRefToAnnouncements < ActiveRecord::Migration[6.1]
  change_table :announcements do |t|
      t.references :category, index: true, foreign_key: true
  end
end
