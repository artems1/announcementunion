class CreateAnimals < ActiveRecord::Migration[6.1]
  def change
    create_table :animals do |t|
      t.string :age
      t.string :gender
      t.string :breed
      t.string :color
      t.references :announcement, foreign_key: true

      t.timestamps null: false
     end 
  end
end
