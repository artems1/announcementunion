class CreateAnnouncements < ActiveRecord::Migration[6.1]
  def change
    create_table :announcements do |t|
      t.text :content
      t.references :user, index: true, foreign_key: true
      #t.references :category, index: true, foreign_key: true
      #t.references :city, index: true, foreign_key: true
      #t.references :animalkind, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :announcements, [:user_id, :created_at]
    
   # add_foreign_key column: :category_id
   # add_foreign_key column: :city_id
   # add_foreign_key column: :animalkind_id
    # add_foreign_key :animals, announcementaddresses, announcementcontacts
    
  end
end