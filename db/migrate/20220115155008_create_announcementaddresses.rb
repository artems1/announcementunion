class CreateAnnouncementaddresses < ActiveRecord::Migration[6.1]
  def change
    create_table :announcementaddresses do |t|
      t.string :street
      t.string :house
      t.string :apartment
      t.references :announcement, foreign_key: true

      t.timestamps null: false
    end
  end
end
