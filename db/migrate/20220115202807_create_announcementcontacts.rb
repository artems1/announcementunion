class CreateAnnouncementcontacts < ActiveRecord::Migration[6.1]
  def change
    create_table :announcementcontacts do |t|
      t.string :firstname
      t.string :email
      t.string :phone
      t.references :announcement, foreign_key: true

      t.timestamps
    end
  end
end
