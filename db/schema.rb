# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_01_17_154821) do

  create_table "animalkinds", force: :cascade do |t|
    t.string "kind"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "animals", force: :cascade do |t|
    t.string "age"
    t.string "gender"
    t.string "breed"
    t.string "color"
    t.integer "announcement_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["announcement_id"], name: "index_animals_on_announcement_id"
  end

  create_table "announcementaddresses", force: :cascade do |t|
    t.string "street"
    t.string "house"
    t.string "apartment"
    t.integer "announcement_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["announcement_id"], name: "index_announcementaddresses_on_announcement_id"
  end

  create_table "announcementcontacts", force: :cascade do |t|
    t.string "firstname"
    t.string "email"
    t.string "phone"
    t.integer "announcement_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["announcement_id"], name: "index_announcementcontacts_on_announcement_id"
  end

  create_table "announcements", force: :cascade do |t|
    t.text "content"
    t.integer "user_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "picture"
    t.integer "category_id"
    t.integer "city_id"
    t.integer "animalkind_id"
    t.index ["animalkind_id"], name: "index_announcements_on_animalkind_id"
    t.index ["category_id"], name: "index_announcements_on_category_id"
    t.index ["city_id"], name: "index_announcements_on_city_id"
    t.index ["user_id", "created_at"], name: "index_announcements_on_user_id_and_created_at"
    t.index ["user_id"], name: "index_announcements_on_user_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string "typeCategory"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "cities", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "favorites", force: :cascade do |t|
    t.integer "announcement_id"
    t.integer "user_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id", "announcement_id"], name: "index_favorites_on_user_id_and_announcement_id", unique: true
    t.index ["user_id"], name: "index_favorites_on_user_id"
  end

  create_table "userinformations", force: :cascade do |t|
    t.string "firstname"
    t.string "secondname"
    t.string "phone"
    t.integer "user_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_userinformations_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "password_digest"
    t.string "remember_digest"
    t.boolean "admin", default: false
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  add_foreign_key "animals", "announcements"
  add_foreign_key "announcementaddresses", "announcements"
  add_foreign_key "announcementcontacts", "announcements"
  add_foreign_key "announcements", "animalkinds"
  add_foreign_key "announcements", "categories"
  add_foreign_key "announcements", "cities"
  add_foreign_key "announcements", "users"
  add_foreign_key "favorites", "users"
  add_foreign_key "userinformations", "users"
end
