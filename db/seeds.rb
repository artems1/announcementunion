# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.create!(name:  "Example User",
             email: "example@announcementunion.com",
             password:              "foobar",
             password_confirmation: "foobar",
             admin: true)
             
Category.create!(typeCategory: "Розыск")
Category.create!(typeCategory: "Покупка")
Category.create!(typeCategory: "Продажа")
Category.create!(typeCategory: "Найден")


City.create!(name: "Москва")
City.create!(name: "Санкт-Петербург")
City.create!(name: "Нижний Новгород")
City.create!(name: "Сочи")
City.create!(name: "Казань")
City.create!(name: "Ростов-на-Дону")

Animalkind.create!(kind: "Кошки")
Animalkind.create!(kind: "Собаки")
Animalkind.create!(kind: "Попугаи")


10.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@announcementunion.com"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password)
end

users = User.order(:created_at).take(6)
10.times do
  content = Faker::Lorem.sentence(word_count: 5)
  users.each { |user| user.announcements.create!(content: content, category_id: 1, city_id: 1, animalkind_id: 1) }
end

users = User.order(:created_at).take(6)
10.times do
  content = Faker::Lorem.sentence(word_count: 5)
  users.each { |user| user.announcements.create!(content: content, category_id: 2, city_id: 2, animalkind_id: 2) }
end

users = User.all

users.each do |user| 
    user.create_userinformation(firstname: "нет данных", secondname: "нет данных") 
end

announcements = Announcement.all


announcements.each do |announcement|
    announcement.create_animal(age: "нет данных", gender: "нет данных", breed: "нет данных", color: "нет данных")
end

announcements.each do |announcement|
    announcement.create_announcementaddress(street: "нет данных", house: "нет данных", apartment: "нет данных")
end

announcements.each do |announcement|
    announcement.create_announcementcontact(firstname: "нет данных", email: "нет данных", phone: "нет данных")
end

#announcements.size do
 # announcements.each { |announcement| announcement.animal.create!() }
#end


