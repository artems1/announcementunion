class Animalkind < ActiveRecord::Base
    
    validates :kind, presence: true
    
    has_many :announcements, dependent: :destroy
    
end
