class Category < ActiveRecord::Base
    
    validates :typeCategory, presence: true
    
    has_many :announcements, dependent: :destroy
end
