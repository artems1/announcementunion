class Announcementcontact < ActiveRecord::Base
  belongs_to :announcement
  
  validates :announcement_id, presence: true
end
