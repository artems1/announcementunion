class Announcementaddress < ActiveRecord::Base
  belongs_to :announcement
  
  validates :announcement_id, presence: true
  
end
