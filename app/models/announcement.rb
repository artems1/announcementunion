class Announcement < ActiveRecord::Base
  belongs_to :user
  belongs_to :category
  belongs_to :city
  belongs_to :animalkind
  
  has_one :animal, dependent: :destroy
  has_one :announcementaddress, dependent: :destroy
  has_one :announcementcontact, dependent: :destroy
  
  #has_many :users, through: :favorites
  
  
  default_scope -> { order(created_at: :desc) }
  mount_uploader :picture, PictureUploader
  validates :user_id, presence: true
  validates :category_id, presence: true
  validates :city_id, presence: true
  validates :animalkind_id, presence: true
  validates :content, presence: true, length: { maximum: 1000 }
  validate  :picture_size
 
  private

  # Проводит валидацию размера загруженного изображения.
  def picture_size
    if picture.size > 5.megabytes
      errors.add(:picture, "should be less than 5MB")
    end
  end

end
