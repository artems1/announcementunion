class FavoritesController < ApplicationController
  before_action :logged_in_user, only: [:new, :destroy, :index]
  before_action :correct_user,   only: [:destroy]
  
  def new
    #debugger
    
    @favorite = current_user.favorites.build(favorite_params)
    if @favorite.save
      #debugger
      flash[:success] = "Favorite created!"
      redirect_to request.referrer || root_url
    else
    redirect_to request.referrer || root_url
    end
  end
  
  
  def index
    
      @categories = Category.all
      @cities = City.all
      @animalkinds = Animalkind.all
      

      myannouncements_id = []
      current_user.favorites.each do |favorite| 
        myannouncements_id.push(favorite.announcement_id)
      end
      @favorites = Announcement.where( id: myannouncements_id ).paginate(page: params[:page])
      #debugger
      
    #Announcement.paginate(page: params[:page])
      
      #announcementCategory = Announcement.where("category_id like ?", params[:category_id])
     # announcementCity = announcementCategory.where("city_id like ?", params[:city_id])
      #@announcements = announcementCity.paginate(page: params[:page])
    
  end
  
  def destroy
    #debugger
    @favorite = Favorite.find(params[:id])
    @favorite.destroy
    flash[:success] = "favorite deleted"
    redirect_to request.referrer || root_url
  end
  
  private
  
  def favorite_params
    params.permit(:announcement_id)
  end
  
  def correct_user
      #debugger

        @favorite = current_user.favorites.find_by(id: params[:id])
        return redirect_to root_url if @favorite.nil?
  end
  
  
end
