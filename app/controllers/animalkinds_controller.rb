class AnimalkindsController < ApplicationController
  before_action :logged_in_user, only: [:index, :create]
  
 def index
    @animalkinds = Animalkind.all
 end
  
    
  
  def show
    @animalkind = Animalkind.find(params[:id])
    @announcements = @animalkind.announcements.paginate(page: params[:page])
    
    @categories = Category.all
    @cities = City.all
    @animalkinds = Animalkind.all
  end
  
  private
  
  def animalkind_params
    params.require(:animalkind).permit(:animalkind)
  end
  
end
  