class AnnouncementaddressesController < ApplicationController
  
  
  before_action :logged_in_user, only: [:create, :destroy, :edit, :update]
  before_action :correct_user,   only: [:destroy, :edit, :update]
  
  
  def new
    @announcementaddress = Announcementaddress.new
  end
  
  
  def create
    @announcementaddress = current_user.announcements.first.build_announcementaddress(announcementaddress_params)
    #debugger
    if @announcementaddress.save
      flash[:success] = "Основная ифнормация про адрес сохранена!"
      redirect_to new_announcementcontact_path
      #root_url
    else
      #@feed_items = []
      render 'static_pages/home'
    end
  end
  
  
  def edit
    #debugger
     @announcement = Announcement.find(params[:id])
    @categories = Category.all
    @cities = City.all
    @animalkinds = Animalkind.all
  end
  
  def destroy
    
    @announcementaddress.destroy
    #flash[:success] = "Animal deleted"
    redirect_to request.referrer || root_url
    
  end
  
  
  def update
    @announcementaddress = Announcementaddress.find(params[:id])
    if @announcementaddress.update(announcementaddress_params)
      #debugger
      # Обрабатывает успешное обновление
      flash[:success] = "Profile updated"
      @announcement = Announcement.find_by(id: @announcementaddress.announcement_id)
       #debugger
      redirect_to @announcement
    else
      render 'edit'
    end
  end
  
  
  private
    
    def announcementaddress_params
      params.require(:announcementaddress).permit(:street, :house, :apartment)
    end
    
    def correct_user
      
      if (params[:action] == "edit")
        if current_user.admin?
          user = Announcement.find(params[:id]).user
          @announcement = user.announcements.find_by(id: params[:id])
        else
          @announcement = current_user.announcements.find_by(id: params[:id])
        end
          return redirect_to root_url if @announcement.nil?
      end
     
     
     if (params[:action] == "update")
       if !current_user.admin?
         user = Announcementaddress.find(params[:id]).announcement.user
         if !(user.id = current_user.id)
           redirect_to root_url
         end
       end
     end
  end
  
  
  
end



  

  
  