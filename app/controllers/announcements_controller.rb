
class AnnouncementsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy, :edit, :update]
  before_action :correct_user,   only: [:destroy, :edit, :update]
  before_action :check_my_info,   only: [:show]


  def create
   #debugger
    @announcement = current_user.announcements.build(announcement_params)
    #debugger
    if @announcement.save
      #animal = @announcement.build_animal
      #animal.save
      #debugger
      flash[:success] = "Основная информация про объявление сохранена"
      redirect_to new_animal_path
      #root_url
    else
      @feed_items = []
      render 'static_pages/home'
    end
  end
  
  def new
    @announcement = Announcement.new
    @categories = Category.all
    @cities = City.all
    @animalkinds = Animalkind.all
  end
  
  def index
    
    @categories = Category.all
    @cities = City.all
    @animalkinds = Animalkind.all
      
    if (params[:category_id].nil? && params[:city_id].nil? && params[:animalkind_id].nil?)
      @announcements = Announcement.paginate(page: params[:page])
    else
      announcementCategory = Announcement.where("cast(category_id as text) like ?", params[:category_id])
      #debugger
      announcementCategoryCity = announcementCategory.where("cast(city_id as text) like ?", params[:city_id])
      announcementCategoryCityAnimalkind = announcementCategoryCity.where("cast(animalkind_id as text) like ?", params[:animalkind_id])
      
      @announcements = announcementCategoryCityAnimalkind.paginate(page: params[:page])
   
      #debugger
    end
    
  end
  
  def edit
    @announcement = Announcement.find(params[:id])
    @categories = Category.all
    @cities = City.all
    @animalkinds = Animalkind.all
  end
  
  def update
    @announcement = Announcement.find(params[:id])
    if @announcement.update(announcement_params)
      # Обрабатывает успешное обновление
      flash[:success] = "Announcement updated"
      redirect_to @announcement
    else
      render 'edit'
    end
  end
  
   def show
     
    @announcement = Announcement.find(params[:id])
    @categories = Category.all
    @cities = City.all
    @animalkinds = Animalkind.all
   end
  
  def destroy
    @announcement.destroy
    flash[:success] = "Announcement deleted"
    redirect_to request.referrer || root_url
  end
  
  def check_my_info
    @announcement = Announcement.find(params[:id])
    
    if @announcement.animal.nil?
       animal = @announcement.build_animal
       animal.save
    end
    if @announcement.announcementaddress.nil?
       announcementaddress = @announcement.build_announcementaddress
       announcementaddress.save
    end
    if @announcement.announcementcontact.nil?
       announcementcontact = @announcement.build_announcementcontact
       announcementcontact.save
    end
    
    #debugger

  end

  private

   def announcement_params
     params.require(:announcement).permit(:content, :picture, :category_id, :city_id, :animalkind_id)
   end
   
    def first_announcement_params
      params.require(:announcement).permit(:category_id)
    end
    
    def correct_user
     
      if current_user.admin?
        user = Announcement.find(params[:id]).user
        @announcement = user.announcements.find_by(id: params[:id])
      else
        @announcement = current_user.announcements.find_by(id: params[:id])
      end
      redirect_to root_url if @announcement.nil?
    end
    
end