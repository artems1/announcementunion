class UsersController < ApplicationController
  
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy]
  before_action :correct_user,   only: [:edit, :update]
  before_action :admin_user,     only: :destroy
  before_action :check_my_info,   only: [:show]

  def index
     #@q   = User.ransack(params[:id])
     #if params[:search].blank?
     # @users = User.first

     #else
      @users = User.paginate(page: params[:page])

     #end
     #@users = @q.paginate(page: params[:page])
     #.result(distinct: true)
  end
  
   def show
    @user = User.find(params[:id])
    @announcements = @user.announcements.paginate(page: params[:page])
    @categories = Category.all
    @cities = City.all
    @animalkinds = Animalkind.all
    #@categories = Category.all
   end
  
  def new
    @user = User.new
    #@userInformation = @user.build_userInformation
  end
  
  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "Пользователь удален"
    redirect_to users_url
  end
  
  def create
    #infoParams = params["user"]["userInformation_attributes"]
    @user = User.new(user_params) 
    if @user.save
      #debugger
      log_in (@user)
      flash[:success] = "Добро пожаловать в AnnouncementUnion!"
      redirect_to new_userinformation_path  #user_url(@user)
      #new_userinformation_path
    else
      render 'new'
    end
  end
  
  def edit
    @user = User.find(params[:id])
  end
  
  def update
    @user = User.find(params[:id])
    if @user.update(user_params)
      # Обрабатывает успешное обновление
      flash[:success] = "Пользователь обновлен"
      redirect_to @user
    else
      render 'edit'
    end
  end
  
    private
    
    def check_my_info
      @user = User.find(params[:id])
      
      if @user.userinformation.nil?
         userinformation = @user.build_userinformation
         userinformation.save
         #debugger
      end
    end
    
    

    def user_params
      #params[:user].except(:userInformation_attributes)
      
    params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
    end
    

    # Подтверждает правильного пользователя
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) if  !current_user?(@user)
    end
    
    # Подтверждает администратора.
    def admin_user
      redirect_to(root_url) if !current_user.admin?
    end

end
