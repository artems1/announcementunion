class AnimalsController < ApplicationController
  
  before_action :logged_in_user, only: [:create, :destroy, :edit, :update]
  before_action :correct_user,   only: [:destroy, :edit, :update]
  before_action :check,   only: [:show]


  def new
    @animal = Animal.new
   # @announcement = Announcement.new
   # @categories = Category.all
    
    #@animal.announcement.build
    #@animal.build_announcement
    
  end

  def create
    @animal = current_user.announcements.first.build_animal(animal_params)
    #debugger
    if @animal.save
      flash[:success] = "Основная информация про живтное сохранена!"
      redirect_to new_announcementaddress_path 
      #root_url
    else
      #@feed_items = []
      render 'static_pages/home'
    end
  end
  
  def edit
     @announcement = Announcement.find(params[:id])
    @categories = Category.all
    @cities = City.all
    @animalkinds = Animalkind.all
  end
  
  def update
    @animal = Animal.find(params[:id])
    if @animal.update(animal_params)
      #debugger
      # Обрабатывает успешное обновление
      flash[:success] = "Profile updated"
      @announcement = Announcement.find_by(id: @animal.announcement_id)
       #debugger
      redirect_to @announcement
    else
      render 'edit'
    end
  end
  
  def destroy
    
    @animal.destroy
    #flash[:success] = "Animal deleted"
    redirect_to request.referrer || root_url
    
  end
  
  
  private
    

    def animal_params
      params.require(:animal).permit(:kind, :age, :gender, :breed, :color)
    end
    
    def correct_user
      
      if (params[:action] == "edit")
        if current_user.admin?
          user = Announcement.find(params[:id]).user
          @announcement = user.announcements.find_by(id: params[:id])
        else
          @announcement = current_user.announcements.find_by(id: params[:id])
        end
          return redirect_to root_url if @announcement.nil?
      end
     
     
     if (params[:action] == "update")
       if !current_user.admin?
         user = Animal.find(params[:id]).announcement.user
         if !(user.id = current_user.id)
           redirect_to root_url
         end
       end
     end
    end
end


    

  
  
