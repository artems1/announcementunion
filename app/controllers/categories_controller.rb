class CategoriesController < ApplicationController
  before_action :logged_in_user, only: [:index, :create]
  
  def new
    @category = Category.new
  end
  
  def create
    @category = Category.new(category_params)
    if @category.save
      flash[:success] = "Category created!"
      redirect_to root_url
    else
      @feed_items = []
      render 'static_pages/home'
    end
  end
  
  def index
    
    @categories = Category.all
    #debugger
  end
  
  def show
    @category = Category.find(params[:id])
    @announcements = @category.announcements.paginate(page: params[:page])
    
    @categories = Category.all
    @cities = City.all
    @animalkinds = Animalkind.all
  end
  
  private
  
  def category_params
    params.require(:category).permit(:typeCategory)
  end
    
  
end