class AnnouncementcontactsController < ApplicationController
  
  before_action :logged_in_user, only: [:create, :destroy, :edit, :update]
  before_action :correct_user,   only: [:destroy, :edit, :update]
  
  
  def new
    @announcementcontact = Announcementcontact.new
  end
  
  
  def create
    @announcementcontact = current_user.announcements.first.build_announcementcontact(announcementcontact_params)
    #debugger
    if @announcementcontact.save
      #flash[:success] = "Основная ифнормация про контакт сохранена"
      redirect_to root_url
    else
      #@feed_items = []
      render 'static_pages/home'
    end
  end
  
  
  def edit
    #debugger
     @announcement = Announcement.find(params[:id])
    @categories = Category.all
    @cities = City.all
    @animalkinds = Animalkind.all
  end
  
  def destroy
    
    @announcementcontact.destroy
    #flash[:success] = "Animal deleted"
    redirect_to request.referrer || root_url
    
  end
  
  
  def update
    @announcementcontact = Announcementcontact.find(params[:id])
    if @announcementcontact.update(announcementcontact_params)
      #debugger
      # Обрабатывает успешное обновление
      flash[:success] = "Profile updated"
      @announcement = Announcement.find_by(id: @announcementcontact.announcement_id)
       #debugger
      redirect_to @announcement
    else
      render 'edit'
    end
  end
  
  
  private
    
    def announcementcontact_params
      params.require(:announcementcontact).permit(:firstname, :email, :phone)
    end
    

  def correct_user
      
      if (params[:action] == "edit")
        if current_user.admin?
          user = Announcement.find(params[:id]).user
          @announcement = user.announcements.find_by(id: params[:id])
        else
          @announcement = current_user.announcements.find_by(id: params[:id])
        end
          return redirect_to root_url if @announcement.nil?
      end
     
     
     if (params[:action] == "update")
       if !current_user.admin?
         user = Announcementcontact.find(params[:id]).announcement.user
         if !(user.id = current_user.id)
           redirect_to root_url
         end
       end
     end
  end
  
  
  
end