class StaticPagesController < ApplicationController
  
  def home
    if logged_in?
      @announcement  = current_user.announcements.build
      @feed_items = current_user.feed.paginate(page: params[:page])
      @categories = Category.all
      @cities = City.all
      @animalkinds = Animalkind.all
    end
  end

  def help
  end
  
  def about
  end
  
  def contact
  end
  
  def search
    @announcement = Announcement.new
    #debugger
    #redirect_to announcements_path(:category_id => params[:category_id], :city_id => params[:city_id])
    
  end
  
  def searchwithparams
    @announcement = Announcement.new
    #debugger
    redirect_to announcements_path(id_params)
    #:category_id => params[:category_id], :city_id => params[:city_id]
  end
  
  def create_new_announcement
    if logged_in?
     @categories = Category.all
    end
  end
  
   def id_params
     params.require(:announcement).permit(:category_id, :city_id, :animalkind_id)
   end

  
end


