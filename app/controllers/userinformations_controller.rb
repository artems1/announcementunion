class UserinformationsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy, :edit, :update]
  before_action :correct_user,   only: [:destroy, :edit, :update]

  
  def new
    #debugger
    @userinformation = Userinformation.new
  end
  
  def create
    #debugger
    @userinformation = current_user.build_userinformation(user_information_params)
    #.build_item_type
    #debugger
    if @userinformation.save
      #debugger
      flash[:success] = "Welcome to the AnnouncementUnion!"
      redirect_to user_url(current_user)
    else
      render 'static_pages/home'
    end
  end
  
  def edit
    @user = User.find(params[:id])
  end
  
  def update
    @userinformation = Userinformation.find(params[:id])
    if @userinformation.update(user_information_params)
      #debugger
      # Обрабатывает успешное обновление
      flash[:success] = "Profile updated"
      redirect_to current_user
    else
      render 'edit'
    end
  end
  
  def destroy
    
    @userinformation.destroy
    #flash[:success] = "Animal deleted"
    redirect_to request.referrer || root_url
    
  end
  
  
  private
    
    def user_information_params
      params.require(:userinformation).permit(:firstname, :secondname, :phone)
    end
      
    
    def correct_user
    
      if !(current_user[:id] == params[:id].to_i)
        redirect_to root_url 
      end
    end
  
end
