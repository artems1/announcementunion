Rails.application.routes.draw do
  
  get 'animalkinds/new'
  get 'favorites/new'
  get 'cities/new'
  get 'announcementcontacts/new'
  get 'announcementaddresses/new'
  get 'userinformations/new'
  get 'userinformation/new'
  get 'animals/new'
  get 'categories/new'
  get 'sessions/new'
  get 'users/new'
  root 'static_pages#home'
  get 'search/announcements' => 'announcements#index'
  
  #get 'static_pages/home'
  get 'help'    => 'static_pages#help'
  get 'about'   => 'static_pages#about'
  get 'contact' => 'static_pages#contact'
  #get 'create_new_announcement' => 'static_pages#create_new_announcement'
  get 'search' => 'static_pages#search'
  get 'signup'  => 'users#new'
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  post   'searchwithparams'   => 'static_pages#searchwithparams'
  delete 'logout'  => 'sessions#destroy'
  #get    'userinformationnew'=> 'userinformation#new'
  resources :users
  resources :announcements,   only: [:destroy, :index, :edit, :update, :new, :create, :show, :search]
  resources :categories,      only: [:index, :show]
  resources :animalkinds,      only: [:index, :show]
  resources :animals,         only: [:destroy, :edit, :new, :show, :index, :create, :update]
  resources :userinformations, only: [:destroy, :edit, :new, :show, :index, :create, :update]
  resources :announcementaddresses, only: [:destroy, :edit, :new, :show, :index, :create, :update]
  resources :announcementcontacts, only: [:destroy, :edit, :new, :show, :index, :create, :update]
  resources :favorites, only: [ :new, :index, :destroy, :create]
#resources :users do
#  resources :announcements
# end
 
#
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'
  # root 'application#hello'
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
